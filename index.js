let
  defaults = require('defaults');
  

function TimeBuckets(opts) {
  this.opts = defaults(opts, {
    // sliding window in miliseconds over which to calculate values
    maxAge: 15 * 60 * 1000,
  
    // if a bucket is over maxAge, we overwrite it with new data
    maxBuckets: 5
  });
  
  
  this.ageBuckets = [];
  
  // initialize buckets
  for(let i = 0; i < this.opts.maxBuckets; i++) {
    this.ageBuckets.push(new Map());
  }

  this.headBucketIdx = 0;
  this.headBucket = this.ageBuckets[this.headBucketIdx];
  this.headBucketExpirationTime = Date.now();
  this.bucketDuration = Math.floor(this.opts.maxAge / this.opts.maxBuckets);
}


TimeBuckets.prototype.observe = function observe(token, value) {
  this.maybeRotateAgeBuckets();
  if(!this.headBucket.has(token)) {
    this.headBucket.set(token, {
      count: 0,
      sum: 0
    });
  }
  
  let bucket = this.headBucket.get(token);
  bucket.count++;
  bucket.sum += value;
}


TimeBuckets.prototype.maybeRotateAgeBuckets = function maybeRotateAgeBuckets() {
  let 
    expTimeShifts = 0,
    now = Date.now();

  // advance expiration time until it's not expired
  while(this.headBucketExpirationTime <= now) {
    this.headBucketExpirationTime += this.bucketDuration;
    expTimeShifts++;
  }
  
  // clear buckets which should be expired based on 
  // how much we had to advance expiration time
  for(let i = 0; i < Math.min(expTimeShifts, this.opts.maxBuckets); i++) {
    this.headBucketIdx++;
    if(this.headBucketIdx >= this.ageBuckets.length) {
      this.headBucketIdx = 0;
    }
    
    this.ageBuckets[this.headBucketIdx].clear();
    this.headBucket = this.ageBuckets[this.headBucketIdx];
  }
}


TimeBuckets.prototype.getValues = function getValues(token) {
  let 
    result = {
      count: 0,
      sum: 0
    };
  
  this.maybeRotateAgeBuckets();
  
  for(let bucketMap of this.ageBuckets) {
    if(bucketMap.has(token)) {
      let bucket = bucketMap.get(token);
      
      result.count += bucket.count;
      result.sum += bucket.sum;
    }
    
  }
  
  return result;
}


module.exports = TimeBuckets;