# Time buckets

Calculates count and sum of values over specified time period using expiring time buckets.

## Example
Count and sum of values over last 450ms split into 3 buckets.  
Each bucket holds 450ms / 3 = 150ms of data and expires after 150ms. Therefore resulting
values represent data anywhere between last 300ms - 450ms (since the bucket which expires contains 150ms of data).

If more granularity is required increase `maxBuckets` size. 

``` js
let TimeBuckets = require('time-buckets');

let tb = new TimeBuckets({
  maxAge: 450,
  maxBuckets: 3
});

tb.observe('counter', 2);
tb.observe('counter', 2);

setTimeout(() => {
  tb.observe('counter', 2);

  let results = tb.getValues('counter');
  assert.ok(results.count === 1);
  assert.ok(results.sum === 2);
}, 500);
```
