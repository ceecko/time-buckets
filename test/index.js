let
  assert = require('assert'),
  TimeBuckets = require('../index');

describe('Counters & sums before expiration', () => {
  it('should work in single bucket', () => {
    let tb = new TimeBuckets({
      maxAge: 450,
      maxBuckets: 3
    });

    tb.observe('counter', 3);
    tb.observe('counter', 3);

    let results = tb.getValues('counter');
    assert.ok(results.count === 2);
    assert.ok(results.sum === 6);
  });


  it('should work in multiple buckets before maxAge', (done) => {
    let tb = new TimeBuckets({
      maxAge: 450,
      maxBuckets: 3
    });

    tb.observe('counter', 2);
    tb.observe('counter', 2);

    setTimeout(() => {
      tb.observe('counter', 2);

      let results = tb.getValues('counter');
      assert.ok(results.count === 3);
      assert.ok(results.sum === 6);
      done();
    }, 200);
  });


  it('should work after maxAge without previous buckets', (done) => {
    let tb = new TimeBuckets({
      maxAge: 450,
      maxBuckets: 3
    });

    tb.observe('counter', 2);
    tb.observe('counter', 2);

    setTimeout(() => {
      tb.observe('counter', 2);

      let results = tb.getValues('counter');
      assert.ok(results.count === 1);
      assert.ok(results.sum === 2);
      done();
    }, 500);
  });

  it('should work after maxAge with previous buckets', (done) => {
    let tb = new TimeBuckets({
      maxAge: 450,
      maxBuckets: 3
    });

    tb.observe('counter', 2);
    tb.observe('counter', 2);

    setTimeout(() => {
      tb.observe('counter', 3);
    }, 200);

    setTimeout(() => {
      tb.observe('counter', 3);

      let results = tb.getValues('counter');
      assert.ok(results.count === 2);
      assert.ok(results.sum === 6);
      done();
    }, 500);
  });

});



